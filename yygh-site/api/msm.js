import request from '@/utils/request'

const api_name = `/api/msm`

//TODO 验证一下是否和后端接口路径一样,mobile的变量
export default {
    sendCode(mobile) {
        return request({
            url: `${api_name}/send/${mobile}`,
            method: `get`
        })
    }
}
