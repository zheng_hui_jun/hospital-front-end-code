import request from '@/utils/request'

const api_name = `/api/ucenter/wx`

//TODO 验证一下是否和后端接口路径一样,mobile的变量
export default {
    //生成微信支付二维码
    createNative(orderId) {
        return request({
            url: `/api/order/weixin/createNative/${orderId}`,
            method: `get`
        })
    },
    getLoginParam() {
        return request({
            url: `${api_name}/getLoginParam`,
            method: `get`
        })
    }
}
